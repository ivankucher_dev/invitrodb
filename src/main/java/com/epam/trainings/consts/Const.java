package com.epam.trainings.consts;

public class Const {

  public static final String USER_TABLE = "invitrousers";
  public static final String USER_ID = "id";
  public static final String USER_FIRSTNAME = "firstname";
  public static final String USER_LASTNAME = "lastname";
  public static final String USER_USERNAME = "username";
  public static final String USER_PASSWORD = "password";
  public static final String USER_LOCATION = "location";
  public static final String USER_GENDER = "gender";

  public static String getInsertString() {
    return "INSERT INTO "
        + Const.USER_TABLE
        + " ("
        + Const.USER_FIRSTNAME
        + ","
        + Const.USER_LASTNAME
        + ","
        + Const.USER_USERNAME
        + ","
        + Const.USER_PASSWORD
        + ","
        + Const.USER_LOCATION
        + ","
        + Const.USER_GENDER
        + ")"
        + " VALUES(?,?,?,?,?,?)";
  }
}
