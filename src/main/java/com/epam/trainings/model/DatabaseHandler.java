package com.epam.trainings.model;

import com.epam.trainings.consts.ConfigConst;
import com.epam.trainings.consts.Const;
import com.epam.trainings.utils.PropertiesReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Properties;

public class DatabaseHandler extends ConfigConst {
  private Connection dbConnection;
  private Properties properties;
  private static Logger log = LogManager.getLogger(PropertiesReader.class.getName());

  public Connection getDbConnection() throws ClassNotFoundException, SQLException {
    properties = PropertiesReader.getPropertiesFile("connection.properties");
    try {
      Class.forName("com.mysql.cj.jdbc.Driver");
      try {
        String connectionString = properties.getProperty("connection");

        dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);
      } catch (SQLException e) {
        log.error("Failed to create db connection");
        e.printStackTrace();
      }
    } catch (ClassNotFoundException e) {
      log.error("Driver not found");
    }
    return dbConnection;
  }

  public void signUpUser(User user) {

    String insert = Const.getInsertString();

    try {

      PreparedStatement prSt = getDbConnection().prepareStatement(insert);
      prSt.setString(1, user.getFirstName());
      prSt.setString(2, user.getLastname());
      prSt.setString(3, user.getUsername());
      prSt.setString(4, user.getPassword());
      prSt.setString(5, user.getLocation());
      prSt.setString(6, user.getGender());
      prSt.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public ResultSet getUser(User user) {
    ResultSet resSet = null;

    String select =
        "SELECT * FROM "
            + Const.USER_TABLE
            + " WHERE "
            + Const.USER_USERNAME
            + " =? AND "
            + Const.USER_PASSWORD
            + " =?";

    try {

      PreparedStatement prSt = getDbConnection().prepareStatement(select);
      prSt.setString(1, user.getUsername());
      prSt.setString(2, user.getPassword());

      resSet = prSt.executeQuery();
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    return resSet;
  }
}
