package com.epam.trainings.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

  private static Logger log = LogManager.getLogger(PropertiesReader.class.getName());

  public static Properties getProperties() {
    Properties prop = null;
    try (InputStream input =
        PropertiesReader.class.getClassLoader().getResourceAsStream("properties/menu.properties")) {

      prop = new Properties();

      if (input == null) {
        log.error("Sorry, unable to find menu.properties");
        return null;
      }
      prop.load(input);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return prop;
  }

  public static Properties getPropertiesFile(String propertiesFilePath) {
    Properties prop = null;
    try (InputStream input =
        PropertiesReader.class.getClassLoader().getResourceAsStream(propertiesFilePath)) {

      prop = new Properties();

      if (input == null) {
        log.error("Sorry, unable to find " + propertiesFilePath);
        return null;
      }
      prop.load(input);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return prop;
  }

  public static String getProperty(String prop) {
    return getProperties().getProperty(prop);
  }

  public static int getIntProp(String prop) {
    return Integer.valueOf(getProperties().getProperty(prop));
  }
}
