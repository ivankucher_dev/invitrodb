package com.epam.trainings.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class Controller {

  @FXML private TextField loginField;

  @FXML private PasswordField passwordField;

  @FXML private Button authSignInButton;

  @FXML private Button loginSignUpButton;

  @FXML
  void initialize() throws Exception {

    loginSignUpButton.setOnAction(event -> {});
  }
}
